<?php

namespace Test\Service;

use Collection\MovieCollection;
use Model\Movie;
use Service\MovieService;
use PHPUnit\Framework\TestCase;
use Util\ArrayUtil;
use Util\ConfigUtil;

/**
 * Class MovieServiceTest
 *
 * @package Test\Service
 */
class MovieServiceTest extends TestCase
{
    private const GENRE = 'animation';
    private const TIME = '12:00';

    /**
     * @return void
     */
    public function testMovieCollectionByGenreAndTime() : void
    {
        $time = $this->getPreparedTime();

        /* @var Movie $movie */
        foreach ($this->getFilteredAndSortedMovieCollection() as $movie) {
            $this->assertContains(self::GENRE, ArrayUtil::arrayToLower($movie->getGenres()));
            $this->assertGreaterThanOrEqual($time, $movie->getFirstAvailableShowing($time));
        }
    }

    /**
     * @return void
     */
    public function testMovieCollectionOrder() : void
    {
        $movieCollection = $this->getFilteredAndSortedMovieCollection();
        $this->assertGreaterThanOrEqual($movieCollection->get(1)->getRating(), $movieCollection->get(0)->getRating());
    }

    /**
     * @return void
     */
    public function testMovieCollectionCount() : void
    {
        $movieCollection = $this->getFilteredAndSortedMovieCollection();
        $this->assertEquals($movieCollection->count(), 2);
    }

    /**
     * @return MovieCollection
     */
    private function getFilteredAndSortedMovieCollection() : MovieCollection
    {
        $movieService = new MovieService(ConfigUtil::getConfig());

        return $movieService->getMovieCollectionByGenreAndTime(self::GENRE, $this->getPreparedTime());
    }

    /**
     * @return \DateTime
     */
    private function getPreparedTime() : \DateTime
    {
        $config = ConfigUtil::getConfig();
        $time = new \DateTime(self::TIME, new \DateTimeZone($config['input_time_timezone']));
        return $time->modify($config['input_time_modify']);
    }
}
