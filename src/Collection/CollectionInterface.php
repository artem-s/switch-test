<?php

namespace Collection;

/**
 * Interface CollectionInterface
 *
 * @package Collection
 */
interface CollectionInterface extends \IteratorAggregate
{
    /**
     * @param mixed $element
     *
     * @return bool
     */
    public function add($element) : bool;

    /**
     * @return void
     */
    public function clear() : void;

    /**
     * @return bool
     */
    public function isEmpty() : bool;

    /**
     * @return int
     */
    public function count() : int;

    /**
     * @param int|string $key
     *
     * @return mixed
     */
    public function get($key);

    /**
     * @param int|string $key
     *
     * @return mixed
     */
    public function remove($key);
}
