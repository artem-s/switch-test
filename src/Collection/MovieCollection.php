<?php

namespace Collection;

use Model\Movie;

/**
 * Class MovieCollection
 *
 * @package Collection
 */
class MovieCollection extends AbstractCollection
{
    /**
     * @param array $data
     *
     * @return MovieCollection
     */
    public function loadFromArray(array $data) : self
    {
        foreach ($data as $item) {
            $this->add(Movie::createInstanceFromArray($item));
        }

        return $this;
    }

    /**
     * @return string
     */
    protected function getModelClass() : string
    {
        return Movie::class;
    }
}
