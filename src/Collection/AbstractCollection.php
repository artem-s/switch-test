<?php

namespace Collection;

use ArrayIterator;
use Exception\InvalidArgumentException;
use Filter\FilterInterface;

/**
 * Class AbstractCollection
 *
 * @package Collection
 */
abstract class AbstractCollection implements CollectionInterface
{
    /** @var array */
    private $elements;

    /**
     * @return string
     */
    abstract protected function getModelClass() : string;

    /**
     * @param mixed $value
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function add($value) : bool
    {
        $modelClass = $this->getModelClass();

        if (!($value instanceof $modelClass)) {
            throw new InvalidArgumentException(sprintf('Expected class %s', $modelClass));
        }

        $this->elements[] = $value;

        return true;
    }

    /**
     * @param int|string $key
     *
     * @return mixed|null
     */
    public function get($key)
    {
        if (!isset($this->elements[$key]) && !array_key_exists($key, $this->elements)) {
            return null;
        }

        return $this->elements[$key];
    }

    /**
     * @param int|string $key
     *
     * @return mixed|null
     */
    public function remove($key)
    {
        if (!isset($this->elements[$key]) && !array_key_exists($key, $this->elements)) {
            return null;
        }

        $removed = $this->elements[$key];
        unset($this->elements[$key]);

        return $removed;
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool
    {
        return empty($this->elements);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->elements);
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator() : ArrayIterator
    {
        return new ArrayIterator($this->elements);
    }

    /**
     * @return void
     */
    public function clear() : void
    {
        $this->elements = [];
    }

    /**
     * @param FilterInterface $filter
     * @param                 $value
     *
     * @return $this
     */
    public function applyFilter(FilterInterface $filter, $value) : self
    {
        $filter->filter($this, $value);

        return $this;
    }

    /**
     * @param string $field
     * @param string $order
     *
     * @return AbstractCollection
     * @throws InvalidArgumentException
     */
    public function orderBy(string $field, $order = 'asc') : self
    {
        $order = strtolower($order);

        if (!in_array($order, ['asc', 'desc'])) {
            throw new InvalidArgumentException('Undefined order direction');
        }

        $method = 'get' . ucfirst($field);
        $modelClass = $this->getModelClass();

        if (!method_exists($modelClass, $method)) {
            throw new InvalidArgumentException(sprintf('Method %s doesn\'t exist in %s ', $method, $modelClass));
        }

        usort($this->elements, function ($first, $second) use ($method, $order) {
            if ($order == 'asc') {
                return $first->$method() > $second->$method();
            }

            return $first->$method() < $second->$method();
        });

        return $this;
    }
}
