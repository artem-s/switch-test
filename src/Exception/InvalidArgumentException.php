<?php

namespace Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Exception
 */
class InvalidArgumentException extends \Exception implements ExceptionInterface
{
}
