<?php

namespace Cli;

use Exception\InvalidArgumentException;

/**
 * Class Cli
 *
 * @package Cli
 */
class Cli
{
    /**
     * @param array $inputArgumentsNames
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function parseInputArguments(array $inputArgumentsNames) : array
    {
        $argv = $_SERVER['argv'];
        unset($argv[0]);

        $inputArguments = [];

        foreach ($inputArgumentsNames as $name) {
            $issetArgument = false;

            foreach ($argv as $argument) {
                if (strripos($argument, $name) !== false) {
                    $inputArguments[] = str_replace('--' . $name . '=', '', $argument);
                    $issetArgument = true;
                }
            }

            if (!$issetArgument) {
                throw new InvalidArgumentException('Please provide ' . count($inputArgumentsNames) . ' arguments: --'
                    . implode(', --', $inputArgumentsNames));
            }
        }

        return $inputArguments;
    }

    /**
     * @param string $time
     * @param string $timezone
     * @param string $modify
     *
     * @return \DateTime
     * @throws InvalidArgumentException
     */
    public function prepareTimeArgument(string $time, string $timezone, string $modify) : \DateTime
    {
        try {
            $timeObject = new \DateTime($time, new \DateTimeZone($timezone));
            return $timeObject->modify($modify);
        } catch (\Throwable $exception) {
            throw new InvalidArgumentException('Please provide input argument "--time" in right time format');
        }
    }
}
