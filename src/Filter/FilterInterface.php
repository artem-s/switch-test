<?php

namespace Filter;

use Collection\CollectionInterface;

/**
 * Interface FilterInterface
 *
 * @package Filter
 */
interface FilterInterface
{
    /**
     * @param CollectionInterface $collection
     * @param mixed               $value
     *
     * @return CollectionInterface
     */
    public function filter(CollectionInterface $collection, $value) : CollectionInterface;
}
