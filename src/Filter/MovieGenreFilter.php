<?php

namespace Filter;

use Collection\CollectionInterface;
use Model\Movie;
use Util\ArrayUtil;

/**
 * Class MovieGenreFilter
 *
 * @package Filter
 */
class MovieGenreFilter implements FilterInterface
{
    /**
     * @param CollectionInterface $collection
     * @param mixed               $value
     *
     * @return CollectionInterface
     */
    public function filter(CollectionInterface $collection, $value) : CollectionInterface
    {
        $value = strtolower($value);

        /* @var Movie $movie */
        foreach ($collection as $key => $movie) {
            if (!in_array($value, ArrayUtil::arrayToLower($movie->getGenres()))) {
                $collection->remove($key);
            }
        }

        return $collection;
    }
}
