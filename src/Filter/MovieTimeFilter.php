<?php

namespace Filter;

use Collection\CollectionInterface;
use Exception\InvalidArgumentException;
use Model\Movie;

/**
 * Class MovieTimeFilter
 *
 * @package Filter
 */
class MovieTimeFilter implements FilterInterface
{
    /**
     * @param CollectionInterface $collection
     * @param                     $value
     *
     * @return CollectionInterface
     * @throws InvalidArgumentException
     */
    public function filter(CollectionInterface $collection, $value) : CollectionInterface
    {
        if (!($value instanceof \DateTime)) {
            throw new InvalidArgumentException(sprintf('Expected class %s', \DateTime::class));
        }

        /* @var Movie $movie */
        foreach ($collection as $key => $movie) {
            $needRemove = true;

            /* @var \DateTime $showing */
            foreach ($movie->getShowings() as $showing) {
                if ($showing >= $value) {
                    $needRemove = false;
                }
            }

            if ($needRemove) {
                $collection->remove($key);
            }
        }

        return $collection;
    }
}
