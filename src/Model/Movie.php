<?php

namespace Model;

/**
 * Class Movie
 *
 * @package Model
 */
class Movie
{
    /** @var string */
    private $name;

    /** @var int */
    private $rating;

    /* @var array */
    private $genres = [];

    /** @var array */
    private $showings = [];

    /**
     * @param array $data
     *
     * @return Movie
     */
    public static function createInstanceFromArray(array $data) : self
    {
        $instance = new self();
        $instance->setName($data['name'])
            ->setRating($data['rating'])
            ->setGenres($data['genres'])
            ->setShowings($data['showings']);

        return $instance;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Movie
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getRating() : int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     *
     * @return Movie
     */
    public function setRating(int $rating) : self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return array
     */
    public function getGenres() : array
    {
        return $this->genres;
    }

    /**
     * @param array $genres
     *
     * @return Movie
     */
    public function setGenres(array $genres) : self
    {
        $this->genres = $genres;

        return $this;
    }

    /**
     * @return array
     */
    public function getShowings() : array
    {
        return $this->showings;
    }

    /**
     * @param \DateTime $time
     *
     * @return \DateTime|null
     */
    public function getFirstAvailableShowing(\DateTime $time) : ?\DateTime
    {
        foreach ($this->getShowings() as $showing) {
            if ($time <= $showing) {
                return $showing;
            }
        }

        return null;
    }

    /**
     * @param array $showings
     *
     * @return Movie
     */
    public function setShowings(array $showings) : self
    {
        $this->showings = [];

        foreach ($showings as $showing) {
            $this->showings[] = new \DateTime($showing);
        }

        return $this;
    }
}
