<?php

namespace Api;

/**
 * Class Client
 *
 * @package Api
 */
class Client
{
    public const METHOD_GET  = 'GET';
    public const METHOD_POST = 'POST';

    /**
     * @param string $url
     * @param string $method
     * @param array  $headerParams
     * @param array  $bodyParams
     *
     * @return array
     */
    public function request(
        string $url,
        string $method = self::METHOD_GET,
        array $headerParams = [],
        array $bodyParams = []
    ) : array {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($headerParams) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerParams);
        }

        if ($method === self::METHOD_POST) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($bodyParams));
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
