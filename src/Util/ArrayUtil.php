<?php

namespace Util;

/**
 * Class ArrayUtil
 *
 * @package Util
 */
class ArrayUtil
{
    /**
     * @param array $array
     *
     * @return array
     */
    public static function arrayToLower(array $array) : array
    {
        $loweredArray = [];

        foreach ($array as $key => $item) {
            $loweredArray[$key] = is_string($item) ? strtolower($item) : $item;
        }

        return $loweredArray;
    }
}
