<?php

namespace Util;

/**
 * Class ConfigUtil
 *
 * @package Util
 */
class ConfigUtil
{
    private const CONFIG_PATH = 'config/config.ini';

    /**
     * @return array
     */
    public static function getConfig() : array
    {
        return parse_ini_file(self::CONFIG_PATH);
    }
}
