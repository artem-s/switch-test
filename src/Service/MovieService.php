<?php

namespace Service;

use Api\Client;
use Collection\MovieCollection;
use Filter\MovieGenreFilter;
use Filter\MovieTimeFilter;

/**
 * Class MovieService
 *
 * @package Service
 */
class MovieService
{
    /** @var array */
    private $config;

    /**
     * MovieService constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string    $genre
     * @param \DateTime $time
     *
     * @return MovieCollection
     */
    public function getMovieCollectionByGenreAndTime(string $genre, \DateTime $time) : MovieCollection
    {
        $apiClient = new Client();
        $arrayResult = $apiClient->request($this->config['endpoint_movie_list']);

        $movieCollection = new MovieCollection();
        $movieCollection->loadFromArray($arrayResult)
            ->applyFilter(new MovieGenreFilter(), $genre)
            ->applyFilter(new MovieTimeFilter(), $time)
            ->orderBy('rating', 'desc');

        return $movieCollection;
    }
}
