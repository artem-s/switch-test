<?php

require_once __DIR__ . '/vendor/autoload.php';

$config = \Util\ConfigUtil::getConfig();


$cli = new Cli\Cli();
try {
    list($genre, $time) = $cli->parseInputArguments(['genre', 'time']);
    $time = $cli->prepareTimeArgument($time, $config['input_time_timezone'], $config['input_time_modify']);
} catch (\Exception $exception) {
    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    exit(1);
}


$movieService = new \Service\MovieService($config);
$movieCollection = $movieService->getMovieCollectionByGenreAndTime($genre, $time);

$output = '';

if (!$movieCollection->isEmpty()) {
    /* @var \Model\Movie $movie */
    foreach ($movieCollection as $movie) {
        $output .= $movie->getName() . ', showing at ' . $movie->getFirstAvailableShowing($time)->format('ga')
            . PHP_EOL;
    }

} else {
    $output = 'no movie recommendations';
}

fwrite(STDOUT, $output . PHP_EOL);
